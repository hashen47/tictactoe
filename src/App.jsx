import { useState } from 'react';
import './App.css'


function Square({
  value, 
  onSquareClick,
  shouldColor,
}) {
  return (
    <button 
      className={`square ${shouldColor ? 'win-sqr' : ''}`}
      onClick={onSquareClick}
    >
      {value}
    </button>
  );
}

function Board({ 
  squares, 
  xIsNext,
  onPlay,
}) {

  let status = '';
  const gameStatus = calculateWinner(squares);

  if (gameStatus.status == 'tie') {
      status = 'Game is Tie';
  } else if (gameStatus.status == 'win') {
      status = `Winner: ${gameStatus.winner}`;
  } else {
    status = `Next: ${xIsNext ? 'X' : '0'}`;
  }

  function handleClick(i) {
    if (squares[i] || calculateWinner(squares).status)
      return;

    const nextSquares = squares.slice();

    if (xIsNext)
      nextSquares[i] = "X";
    else
      nextSquares[i] = "O";

    onPlay(nextSquares);
  }


  const squareRowList = [0, 3, 6].map(n => {
    return (
      <div 
        key={n}
        className="board-row"
      >
        {
          [0, 1, 2].map(i => {
            return (
              <Square 
                key={i + n}
                shouldColor={Object.values(gameStatus.pos).includes(i + n)}
                value={squares[i + n]} 
                onSquareClick={() => handleClick(i + n)} 
              />
            )
          })
        }
      </div>                                       
    );
  });


  return (
    <>
      <div className="status">{status}</div>
      {squareRowList}
    </>
  );
}


export default function Game() {
  const [history, setHistory] = useState([Array(9).fill(null)]);
  const [currentMove, setCurrentMove] = useState(0);
  const [orderIsAsc, setOrderIsAsc] = useState(true);
  const squares = history[currentMove];
  const xIsNext = currentMove % 2 === 0;

  function handlePlay(nextSquares) {
    const nextHistory = [...history.slice(0, currentMove + 1), nextSquares];
    setHistory(nextHistory);
    setCurrentMove(nextHistory.length - 1);
  }

  function jumpTo(move) {
    setCurrentMove(move);
  }

  function handleSort(value) {
    setOrderIsAsc(value);
  }

  let historyDuplicate = history.slice();
  if (!orderIsAsc) {
    historyDuplicate.reverse();
  }

  const moves = historyDuplicate.map((_, i) => {
    let move;
    let description;

    if (!orderIsAsc) {
      move = history.length - 1 - i;
    } else {
      move = i;
    }

    if (move > 0) 
      description = `Go to move # ${move}`;
    else
      description = 'Go to game start';

    return (
      <li
        key={move}
      >
        {
          (currentMove === move)
          ? description
          :
          <button 
            onClick={() => jumpTo(move)}
          >{description}</button>
        }
      </li>
    );
  });

  return (
    <div className="game">
      <div className="game-board">
        <Board 
          squares={squares}
          xIsNext={xIsNext}
          onPlay={handlePlay}
        />
      </div>
      <div className="game-history">
        <ul>
          <li>
            <button
              className={`sort-btn ${orderIsAsc ? 'desc' : ''}`}
              onClick={() => handleSort(orderIsAsc ? false : true)}
            >
              {
                orderIsAsc ?
                'Desc'
                : 'Asc'
              }
            </button>
          </li>
        </ul>
        <ol>
          {moves}
        </ol>
      </div>
    </div>
  );
}

function calculateWinner(squares) {
  const winnerState = {
    status: null,
    winner: null,
    pos: {
      x: null,
      y: null,
      z: null,
    }
  };
  const positions = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [2, 4, 6], 
    [0, 4, 8],
  ];

  for (let i = 0; i < positions.length; i++) {
    const [x, y, z] = positions[i];
    if (squares[x] && squares[x] === squares[y] && squares[x] === squares[z]) {
      winnerState.status = 'win';
      winnerState.winner = squares[x];
      winnerState.pos.x = x;
      winnerState.pos.y = y;
      winnerState.pos.z = z;
      break;
    }
  }

  if (!squares.includes(null) && winnerState.status == null) {
    winnerState.status = 'tie';
  }

  return winnerState;
}
